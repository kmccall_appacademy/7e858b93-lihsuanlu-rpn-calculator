class RPNCalculator
  # TODO: your code goes here!
  attr_accessor :array, :operators_arr, :value,:op_count,:num_count, :flag
  def initialize
    @array = []
  end
  def push(ele)
    @array.push(ele)
  end
  def plus
    perform_operation(:+)
  end

  def minus
    perform_operation(:-)
  end
  def times
    perform_operation(:*)
  end
  def divide
    perform_operation(:/)
  end

  def tokens(string)
    string = string.split
    string.map{ |char| operation_include?(char) ? char.to_sym : char.to_i}
  end

  def evaluate(string)
    token = tokens(string)
    token.each do |char|
      if operation_include?(char.to_s)
        perform_operation(char)
      else
        push(char)
      end
    end
    value
  end
  def value
    array.last
  end

  private

  def operation_include?(char)
    [:+,:-,:*,:/].include?(char.to_sym)
  end

  def perform_operation(op_symbol)
    raise "calculator is empty" if array.length < 2
    first_operand = array.pop
    second_operand = array.pop
    case op_symbol
    when :+
      array << first_operand+second_operand
    when :-
      array << second_operand-first_operand
    when :*
      array << first_operand*second_operand
    when :/
      array << second_operand.fdiv(first_operand)
    end

  end


end
